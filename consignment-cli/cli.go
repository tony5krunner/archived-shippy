package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"

	micro "github.com/micro/go-micro"
	"github.com/spf13/cobra"
	pb "gitlab.com/phuonghuynh-eng/shippy/consignment-service/proto/consignment"
)

const (
	address         = "localhost:50051"
	defaultFilename = "consignment.json"
)

var rootCmd = &cobra.Command{
	Use:   "consignment-cli",
	Short: "Consignment CLI",
}

func parseFile(file string) (*pb.Consignment, error) {
	consignment := new(pb.Consignment)
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(data, consignment)
	return consignment, err
}

func makeClient() pb.ShippingServiceClient {
	service := micro.NewService(micro.Name("shippy.consignment.cli"))
	service.Init()

	return pb.NewShippingServiceClient("shippy.service.consignment", service.Client())
}

var createCmd = &cobra.Command{
	Use:   "create FILENAME",
	Short: "Create new consignment",
	Run: func(cmd *cobra.Command, args []string) {
		filename := defaultFilename
		if len(args) > 0 {
			filename = args[0]
		}

		client := makeClient()

		consignment, err := parseFile(filename)
		if err != nil {
			log.Fatalf("could not parse file: %v", err)
		}

		if consignment == nil {
			log.Fatal("nil consignment")
		}

		r, err := client.CreateConsignment(context.Background(), consignment)
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		log.Printf("created: %t", r.Created)
	},
}

var getCmd = &cobra.Command{
	Use:   "getall",
	Short: "Get all consignments",
	Run: func(cmd *cobra.Command, args []string) {
		client := makeClient()
		getAllResp, err := client.GetConsignments(context.Background(), &pb.GetRequest{})
		if err != nil {
			log.Fatalf("could not list consignments: %v", err)
		}
		for _, v := range getAllResp.Consignments {
			log.Println(v)
		}
	},
}

func main() {

	rootCmd.AddCommand(createCmd)
	rootCmd.AddCommand(getCmd)

	rootCmd.Execute()
}
